package ru.shumov.tm.entity;

import ru.shumov.tm.enums.Role;

import java.util.UUID;

public class User {

    private Role role;
    private String password;
    private String login;
    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "role=" + role.getDisplayName() +
                ", login='" + login + '\'' +
                '}';
    }
}

