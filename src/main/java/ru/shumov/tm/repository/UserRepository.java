package ru.shumov.tm.repository;

import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private Utils utils = new Utils();

    private Map<String, User> users = new HashMap<>();

    public void persist(User user) {
        if(!users.containsKey(user.getLogin())) {
            users.put(user.getLogin(), user);
        }
    }

    public void merge(User user) {
        users.put(user.getId(), user);
    }

    public Collection<User> findAll() {
        return users.values();
    }

    public User findOne(String login) {
        return users.get(login);
    }

    public void usersCreate() throws NoSuchAlgorithmException {
        User admin = new User();
        admin.setLogin("admin");
        admin.setRole(Role.ADMINISTRATOR);
        String adminPassword = "admin";
       adminPassword = utils.md5(adminPassword);
        admin.setPassword(adminPassword);
        users.put(admin.getLogin(), admin);
        User user = new User();
        user.setRole(Role.USER);
        user.setLogin("user");
        String userPassword = "user";
        userPassword = utils.md5(userPassword);
        user.setPassword(userPassword);
        users.put(user.getLogin(), user);
    }
}
