package ru.shumov.tm.exceptions;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

public class IncorrectInput extends Exception{

    private String massage;

    public IncorrectInput(String s) {
        this.massage = s;
    }

    public String getMassage(){
        return massage;
    }
}
