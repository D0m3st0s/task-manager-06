package ru.shumov.tm.command;

import ru.shumov.tm.enums.Role;

import java.security.NoSuchAlgorithmException;

public abstract class AbstractCommand {
    private String name;
    private String description;
    private Role role;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


    public abstract void execute() throws NoSuchAlgorithmException;
}
