package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.util.Data;

public class UserLogOutCommand extends AbstractCommand {

    private Data data;
    private Bootstrap bootstrap;
    private String name = "log out";
    private String description = "log out: Выход из учётной записи";

    public UserLogOutCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute()  {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.USER_DID_NOT_AUTHORIZED);
            return;
        }
        bootstrap.authorizationResult(null);
        data.outPutString(Commands.LOG_OUT_SUCCESSFUL);
    }
}
