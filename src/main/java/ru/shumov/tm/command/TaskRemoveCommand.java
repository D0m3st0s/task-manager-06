package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

public class TaskRemoveCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "task remove";
    private String description = "task remove: Выборочное удаление задач.";

    public TaskRemoveCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        final var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            data.outPutString(Commands.ENTER_TASK_ID_FOR_REMOVING);
            final var taskId = data.scanner();
            final var task = bootstrap.getTaskService().getOne(taskId);
            if(task == null) {
                data.outPutString(Commands.TASK_DOES_NOT_EXIST);
                return;
            }
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTaskService().remove(taskId);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
