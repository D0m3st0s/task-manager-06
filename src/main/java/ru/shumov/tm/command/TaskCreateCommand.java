package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "task create";
    private String description = "task create: Создание нового задания.";

    public TaskCreateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            data.outPutString(Commands.ENTER_PROJECT_ID_FOR_TASKS);
            final var projectId = data.scanner();
            try {
                if (bootstrap.getProjectService().checkKey(projectId)) {
                    data.outPutString(Commands.ENTER_TASK_NAME);
                    final var name = data.scanner();
                    Task task = new Task();
                    final var userId = bootstrap.getUser().getId();
                    data.outPutString(Commands.ENTER_START_DATE_OF_TASK);
                    final var startDateS = data.scanner();
                    Date StartDateD = format.parse(startDateS);
                    data.outPutString(Commands.ENTER_DEADLINE_OF_TASK);
                    final var endDateS = data.scanner();
                    Date endDateD = format.parse(endDateS);
                    final var id = UUID.randomUUID().toString();
                    data.outPutString(Commands.ENTER_DESCRIPTION_OF_TASK);
                    final var description = data.scanner();

                    task.setDescription(description);
                    task.setEndDate(endDateD);
                    task.setStartDate(StartDateD);
                    task.setProjectId(projectId);
                    task.setName(name);
                    task.setId(id);
                    task.setUserId(userId);

                    bootstrap.getTaskService().create(task);
                } else {
                    data.outPutString(Commands.PROJECT_DOES_NOT_EXIST);
                }
            } catch (ParseException parseException) {
                data.outPutString(Commands.INCORRECT_DATE_FORMAT);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
