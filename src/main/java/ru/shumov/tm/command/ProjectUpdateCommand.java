package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;


import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ProjectUpdateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "project update";
    private String description = "project update: Изменение параметров проекта.";

    public ProjectUpdateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var user = bootstrap.getUser();
        var project = new Project();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            data.outPutString(Commands.ENTER_ID_OF_PROJECT_FOR_SHOWING);
            final var projectId = data.scanner();
            if(!bootstrap.getProjectService().checkKey(projectId)) {
                data.outPutString(Commands.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if(bootstrap.getProjectService().checkKey(projectId)) {
                project = bootstrap.getProjectService().getOne(projectId);
            }
            if(!project.getUserId().equals(user.getId())) {
                data.outPutString(Commands.NO_ROOTS_FOR_PROJECT);
                return;
            }
            try {
                System.out.println();
                final var name = data.scanner();
                System.out.println();
                final var description = data.scanner();
                System.out.println();
                final var startDate = data.scanner();
                System.out.println();
                final var endDate = data.scanner();
                project.setName(name);
                project.setDescription(description);
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                bootstrap.getProjectService().update(project);
            } catch (ParseException parseException) {
                data.outPutString(Commands.INCORRECT_DATE_FORMAT);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
