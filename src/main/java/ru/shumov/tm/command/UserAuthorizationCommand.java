package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.util.Data;
import ru.shumov.tm.util.Utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserAuthorizationCommand extends AbstractCommand {

    private Data data;
    private Bootstrap bootstrap;
    private Utils utils;
    private String name = "log in";
    private String description = "log in: Авторизация пользователя.";

    public UserAuthorizationCommand(Data data, Bootstrap bootstrap, Utils utils) {
        this.data = data;
        this.bootstrap = bootstrap;
        this.utils = utils;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        var user = bootstrap.getUser();
        if(user != null) {
            data.outPutString(Commands.USER_ALREADY_AUTHORIZED);
            return;
        }
        data.outPutString(Commands.ENTER_NAME_OF_USER);
        var login = data.scanner();
         user = bootstrap.getUserService().getOne(login);
        if(user != null) {
            data.outPutString(Commands.ENTER_PASSWORD);
            final var password = data.scanner();
            final var output = utils.md5(password);
            if(output.equals(user.getPassword())) {
                bootstrap.authorizationResult(user);
                data.outPutString(Commands.AUTHORIZATION_SUCCESSFUL);
            } else {
                data.outPutString(Commands.INVALID_PASSWORD);
            }
        } else {
            data.outPutString(Commands.INVALID_LOGIN);
        }
    }
}
