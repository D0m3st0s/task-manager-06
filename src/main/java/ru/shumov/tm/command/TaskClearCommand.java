package ru.shumov.tm.command;


import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

public class TaskClearCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.ADMINISTRATOR;
    private String name = "task clear";
    private String description = "task clear: Удаление всех задач.";

    public TaskClearCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            data.outPutString(Commands.ALL_TASKS_WILL_BE_CLEARED);
            data.outPutString(Commands.YES_NO);
            final var answer = data.scanner();
            if (Commands.YES.equals(answer)) {
                bootstrap.getTaskService().clear();
            }
        }  else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
