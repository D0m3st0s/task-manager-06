package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;


import java.util.Collection;

public class ProjectRemoveCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "project remove";
    private String description = "project remove: Выборочное удаление проектов.";

    public ProjectRemoveCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            data.outPutString(Commands.ENTER_PROJECT_ID_FOR_REMOVING);
            var projectId = data.scanner();
            final var project = bootstrap.getProjectService().getOne(projectId);
            if(project == null) {
                data.outPutString(Commands.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if (project.getUserId().equals(user.getId())) {
                bootstrap.getProjectService().remove(projectId);
            } else {
                data.outPutString(Commands.NO_ROOTS);
                return;
            }
            Collection<Task> values = bootstrap.getTaskService().getList();
            for (Task task : values) {
                if (task.getProjectId().equals(projectId) && task.getUserId().equals(user.getId())) {
                    values.remove(task);
                }
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
