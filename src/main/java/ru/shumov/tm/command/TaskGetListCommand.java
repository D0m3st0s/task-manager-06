package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

import java.util.Collection;

public class TaskGetListCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "task list";
    private String description = "task list: Вывод всех задач.";

    public TaskGetListCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var counter = 0;
        final var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            Collection<Task> values = bootstrap.getTaskService().getList();
            if (values.isEmpty()) {
                data.outPutString(Commands.TASKS_DO_NOT_EXIST);
                return;
            }
            for(Task task: values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if(counter == 0) {
                data.outPutString(Commands.TASKS_DO_NOT_EXIST);
                return;
            }
            for (Task task : values) {
                if (task.getUserId().equals(bootstrap.getUser().getId())) {
                        data.outPutTask(task);
                }
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
