package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;


import java.util.Collection;

public class ProjectGetListCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "project list";
    private String description = "project list: Вывод всех проектов.";
    private final boolean secure = false;

    public ProjectGetListCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        int counter = 0;
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            Collection<Project> values = bootstrap.getProjectService().getList();
            if (values.isEmpty()) {
                data.outPutString(Commands.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for(Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                    counter++;
                }
            }
            if(counter == 0) {
                data.outPutString(Commands.PROJECTS_DO_NOT_EXIST);
                return;
            }
            for (Project project : values) {
                if (project.getUserId().equals(bootstrap.getUser().getId())) {
                        data.outPutProject(project);
                }
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
