package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand{

    private Data data;
    private Role role = Role.USER;
    private Bootstrap bootstrap;
    private String name = "project create";
    private String description = "project create: Создание нового проекта.";

    public ProjectCreateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            try {
                var format = new SimpleDateFormat();
                format.applyPattern("dd.MM.yyyy");
                data.outPutString(Commands.ENTER_PROJECT_NAME);
                final var name = data.scanner();
                Project project = new Project();
                final var userId = bootstrap.getUser().getId();
                final var id = UUID.randomUUID().toString();
                data.outPutString(Commands.ENTER_START_DATE_OF_PROJECT);
                final var startDate = data.scanner();
                data.outPutString(Commands.ENTER_DEADLINE_OF_PROJECT);
                final var endDate = data.scanner();
                data.outPutString(Commands.ENTER_DESCRIPTION_OF_PROJECT);
                final var description = data.scanner();

                project.setDescription(description);
                project.setName(name);
                project.setStartDate(format.parse(startDate));
                project.setEndDate(format.parse(endDate));
                project.setId(id);
                project.setUserId(userId);
                bootstrap.getProjectService().create(project);

            } catch (ParseException parseException) {
                data.outPutString(Commands.INCORRECT_DATE_FORMAT);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
