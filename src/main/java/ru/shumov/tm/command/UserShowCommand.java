package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

import java.security.NoSuchAlgorithmException;

public class UserShowCommand extends AbstractCommand {

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "show";
    private String description = "show: Вывод информации о пользователе.";

    public UserShowCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    public Role getRole() {return role;}

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.USER_DID_NOT_AUTHORIZED);
            return;
        }
        data.outPutUser(user);
    }
}
