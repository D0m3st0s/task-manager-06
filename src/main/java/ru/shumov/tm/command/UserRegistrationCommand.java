package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;
import ru.shumov.tm.util.Utils;

import java.security.NoSuchAlgorithmException;

public class UserRegistrationCommand extends AbstractCommand {

    private Data data;
    private Bootstrap bootstrap;
    private Utils utils;
    private String name = "registration";
    private String description = "registration: Регистрация нового пользователя.";

    public UserRegistrationCommand(Data data, Bootstrap bootstrap, Utils utils) {
        this.data = data;
        this.bootstrap = bootstrap;
        this.utils = utils;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        var user = bootstrap.getUser();
        if(user != null) {
            return;
        }
        data.outPutString(Commands.ENTER_LOGIN);
        final var login = data.scanner();
        if(bootstrap.getUserService().getOne(login) != null){
            data.outPutString(Commands.USER_ALREADY_EXIST);
        } else {
            user = new User();
            data.outPutString(Commands.ENTER_PASSWORD);
            final var password = data.scanner();
            if(password == null || password.isEmpty()) {
                data.outPutString(Commands.INVALID_PASSWORD);
                return;
            }
            final var output = utils.md5(password);
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPassword(output);
            bootstrap.getUserService().create(user);
            data.outPutString(Commands.REGISTRATION_SUCCESSFUL);
        }
    }
}
