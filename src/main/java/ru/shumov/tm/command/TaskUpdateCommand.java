package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TaskUpdateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "task update";
    private String description = "task update: Изменения параметров задачи.";

    public TaskUpdateCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        final var user = bootstrap.getUser();
        var task = new Task();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            var format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            data.outPutString(Commands.ENTER_ID_OF_TASK_FOR_SHOWING);
            final var taskId = data.scanner();
            if(!bootstrap.getTaskService().checkKey(taskId)) {
                data.outPutString(Commands.PROJECT_DOES_NOT_EXIST);
                return;
            }
            if(bootstrap.getTaskService().checkKey(taskId)) {
                task = bootstrap.getTaskService().getOne(taskId);
            }
            if(!task.getUserId().equals(user.getId())) {
                data.outPutString("У вас нет доступа к этой задаче.");
                return;
            }
            try {
                data.outPutString(Commands.ENTER_TASK_NAME);
                final var name = data.scanner();
                data.outPutString(Commands.ENTER_DESCRIPTION_OF_TASK);
                final var description = data.scanner();
                data.outPutString(Commands.ENTER_START_DATE_OF_TASK);
                final var startDate = data.scanner();
                data.outPutString(Commands.ENTER_DEADLINE_OF_PROJECT);
                final var endDate = data.scanner();
                task.setName(name);
                task.setDescription(description);
                task.setStartDate(format.parse(startDate));
                task.setEndDate(format.parse(endDate));
                bootstrap.getTaskService().update(task);
            } catch (ParseException parseException) {
                data.outPutString(Commands.INCORRECT_DATE_FORMAT);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
