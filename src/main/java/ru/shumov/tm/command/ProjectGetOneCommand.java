package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;


public class ProjectGetOneCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Role role = Role.USER;
    private String name = "get project";
    private String description = "get project: Вывод конкретного проекта.";

    public ProjectGetOneCommand(Data data, Bootstrap bootstrap) {
        this.data = data;
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        User user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        if(user.getRole().equals(role)) {
            data.outPutString(Commands.ENTER_PROJECT_ID);
            final var projectId = data.scanner();
            final var project = bootstrap.getProjectService().getOne(projectId);
            if (project.getUserId().equals(user.getId())) {
                data.outPutProject(project);
            } else {
                data.outPutString(Commands.NO_ROOTS);
            }
        } else {
            data.outPutString(Commands.NO_ROOTS);
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
