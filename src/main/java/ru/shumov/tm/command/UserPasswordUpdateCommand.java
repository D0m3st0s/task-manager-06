package ru.shumov.tm.command;

import ru.shumov.tm.Bootstrap;
import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.util.Data;
import ru.shumov.tm.util.Utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserPasswordUpdateCommand extends AbstractCommand{

    private Data data;
    private Bootstrap bootstrap;
    private Utils utils;
    private Role role = Role.USER;
    private String name = "password update";
    private String description = "password update: Изменение пароля пользователя.";

    public UserPasswordUpdateCommand(Data data, Bootstrap bootstrap, Utils utils) {
        this.data = data;
        this.bootstrap = bootstrap;
        this.utils = utils;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void execute() throws NoSuchAlgorithmException {
        var user = bootstrap.getUser();
        if(user == null) {
            data.outPutString(Commands.NO_ROOTS);
            return;
        }
        data.outPutString(Commands.ENTER_OLD_PASSWORD);
        final var oldPassword = data.scanner();
        final var output = utils.md5(oldPassword);
        if(output.equals(user.getPassword())) {
            data.outPutString(Commands.ENTER_NEW_PASSWORD);
            var newPassword = data.scanner();
            final var password = utils.md5(newPassword);
            user.setPassword(password);
            bootstrap.getUserService().update(user);
            data.outPutString(Commands.PASSWORD_UPDATE_SUCCESSFUL);
        } else {
            data.outPutString(Commands.INVALID_PASSWORD);
        }
    }
}
