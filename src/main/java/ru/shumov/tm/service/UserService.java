package ru.shumov.tm.service;


import ru.shumov.tm.entity.User;
import ru.shumov.tm.repository.UserRepository;

import java.util.Collection;

public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Collection<User> getList() {
        return userRepository.findAll();
    }

    public User getOne(String login) {
        return userRepository.findOne(login);
    }

    public void create(User user) {
        try {
            if (user.getLogin() == null || user.getLogin().isEmpty()) {
                throw new NullPointerException();
            }
        }
        catch (NullPointerException exception){
            exception.printStackTrace();
        }
        userRepository.persist(user);
    }

    public void update(User user) {
        userRepository.merge(user);
    }
}
