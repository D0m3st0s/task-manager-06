package ru.shumov.tm.service;

import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.exceptions.IncorrectInput;
import ru.shumov.tm.repository.ProjectRepository;
import ru.shumov.tm.util.Data;

import java.util.Collection;

public class ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;
    private Data data;

    public ProjectService(ProjectRepository projectRepository, Data data) {
        this.projectRepository = projectRepository;
        this.data = data;
    }

    public void create(Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.persist(project.getId(), project);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public void clear() {
        projectRepository.removeAll();
        taskService.clear();
    }

    public void remove(String projectId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_PROJECT + "project id");
            }
            projectRepository.remove(projectId);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public Collection<Project> getList() {
        Collection<Project> values = projectRepository.findAll();
        return values;
    }

    public Project getOne(String id) {
        return projectRepository.findOne(id);
    }

    public void update(Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.merge(project);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public boolean checkKey(String id) {
        if(projectRepository.findOne(id) != null) {
            return projectRepository.findOne(id).getId().equals(id);
        }
        return false;
    }
}
