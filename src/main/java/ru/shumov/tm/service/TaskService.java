package ru.shumov.tm.service;

import ru.shumov.tm.Commands;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInput;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.util.Data;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

public class TaskService {
    private Data data;
    private TaskRepository taskRepository;

    public TaskService(TaskRepository bootstrapTaskRepository, Data data) {
        this.taskRepository = bootstrapTaskRepository;
        this.data = data;
    }

    public void create(Task task) {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_TASK + "project id");
            }
            taskRepository.persist(task);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public void clear() {
        taskRepository.removeAll();
    }

    public void remove(String taskId) {
        taskRepository.remove(taskId);
    }

    public Collection<Task> getList() {
        Collection<Task> value = taskRepository.findAll();
        return value;
    }

    public Task getOne(String id) {
        return taskRepository.findOne(id);
    }

    public void update(Task task) throws ParseException {
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInput(Commands.EXCEPTION_TASK + "project id");
            }
            taskRepository.merge(task);
        }
        catch (IncorrectInput incorrectInput) {
            data.outPutString(incorrectInput.getMassage());
        }
    }

    public boolean checkKey(String id) {
        if(taskRepository.findOne(id) != null) {
            return taskRepository.findOne(id).getId().equals(id);
        }
        return false;
    }
}
