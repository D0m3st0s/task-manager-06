package ru.shumov.tm;

import java.security.NoSuchAlgorithmException;

public class Application {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.commandsInit();
        bootstrap.start();
    }
}
